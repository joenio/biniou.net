const PRODUCTIONS_V2 = {
  'DJ Manu Kebab': [
    {
      name: 'GhostInTheShell Drum&Bass',
      video: 'https://manu-kebab.fr/20220120-GhostInTheShell-DrumnBass/20220120-ManuKebab-GhostInTheShell-DrumnBass-1080p.mp4'
    },
    {
      name: 'Jazzy Drum&Bass',
      video: 'https://manu-kebab.fr/20210312-Jazzy-DrumnBass/20210312-ManuKebab-Jazzy-DrumnBass-1080p.mp4'
    },
    {
      name: 'Minimal Techno',
      video: 'https://manu-kebab.fr/20210314-Minimal-Techno/20210314-ManuKebab-Minimal-Techno-1080p.mp4'
    },
    {
      name: 'Visual Trip',
      video: 'https://manu-kebab.fr/20210413-VisualTrip-Techno/20210413-ManuKebab-VisualTrip-Techno-1080p.mp4'
    }
  ],
  'Artesian Dream': [
    {
      name: 'Ghosts',
      video: 'https://dl.biniou.net/biniou.net/productions/Artesian%20Dream%20-%20Ghosts.mp4'
    },
    {
      name: 'RADD 18 (Artesian Dream Remix)',
      youtube: 'OBIEz75EAFU'
    }
  ],
  JuF: [
    {
      name: 'Half-Life Period (Dox Version)',
      youtube: 'I1JgsPgXJpA'
    },
    {
      name: 'Silent Force (Dox Version)',
      youtube: 'll5u7q_mvyk'
    },
    {
      name: 'Wie die Sonne',
      youtube: 'UidBCiszxAE'
    },
    {
      name: 'Cosmic Dance',
      youtube: 'ArimnXuLilQ'
    }

  ],
  'The Splashdowns': [
    {
      name: 'Live Medley No.1',
      youtube: 'z__5GuZfRH4'
    },
    {
      name: 'East- West Symposium',
      youtube: 'JIf2oocv36Y'
    }
  ],
  Tavasti: [
    {
      name: 'Alder Blooming',
      youtube: 'ycVrgGtrBmM'
    },
    {
      name: 'r1 158963657',
      youtube: 'xiqjF23YOok'
    }
  ],
  'Lilith 93': [
    {
      name: 'Cadmean Nights',
      youtube: '6DcsqBRBIew'
    }
  ],
  'DJ DRMR': [
    {
      name: '2021 IFM Streaming Fest',
      video: 'https://hotmixes.net/mixes/i/intergalactic.fm/2021.streaming.festival/video/drmr.cbs.mp4'
    }
  ],
  Oliv3: [
    {
      name: 'Paddy Glackin - An Droighnean Donn',
      youtube: 'pECQY1Rg2sw'
    },
    {
      name: 'Davy Spillane - Lament for the dead of the north',
      youtube: '7j_VoknTpLg'
    },
    {
      name: 'John Sheahan - Carolan\'s devotion',
      youtube: '2KEFIAJ6Gyc'
    },
    {
      name: 'Éric Marienthal',
      youtube: 'RdceMJ1JFHM'
    }
  ],
  'The Total Chaos': [
    {
      name: 'The girl with the stars in her eyes',
      youtube: 'P8lwJTv6wg0'
    }
  ],
  mArdək: [
    {
      name: 'Event Horizon',
      youtube: 'PPSSDkxWO_8'
    },
    {
      name: 'Bonne Nuit',
      youtube: 'zjNGBpOBSfs'
    }
  ],
  Lenain: [
    {
      name: 'Banditos - Stargazouille',
      youtube: 'Q_fSbWtzTAM'
    },
    {
      name: 'Bernie - Le Cauchemar',
      youtube: 'G8mriJOfnwA'
    }
  ]
}

export default PRODUCTIONS_V2
